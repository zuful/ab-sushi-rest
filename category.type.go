package main

type Category struct {

	Id 		string	`json:"id,omitempty"`
	Name 	string	`json:"name,omitempty"`
	Pieces 	[]*Piece	`json:"pieces,omitempty"`

}

