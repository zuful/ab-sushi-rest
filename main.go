package main

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-contrib/cors"
	"firebase.google.com/go"
	"context"
	"cloud.google.com/go/firestore"
	"google.golang.org/api/option"
	"log"
)

var stringConnect string

var usersCollectionName string = "users"
var restaurantsCollectionName string = "restaurants"
var mealsCollectionName string = "meals"
var ordersCollectionName string = "orders"

var app				*firebase.App
var firebaseErr	 	error
var firestoreClient *firestore.Client
var ctx				context.Context


func main(){
	r := gin.Default()

	// CORS for https://foo.com and https://github.com origins, allowing:
	// - PUT and PATCH methods
	// - Origin header
	// - Credentials share
	// - Preflight requests cached for 12 hours

	r.Use(cors.New(cors.Config{
		AllowAllOrigins: true,
		AllowHeaders:[]string{"*"},
	}))

	r.GET("/ping", func(c *gin.Context){
		c.String(200, "pong")
	})

	//User
	r.GET("/user/:userId", GetUserHandler)
	r.POST("/user", CreateUserHandler)
	r.PATCH("/user", UpdateUserHandler)

	//Restaurant
	r.GET("/restaurant/:restaurantId", GetRestaurantHandler)
	r.GET("/all-restaurants", GetAllRestaurantsHandler)
	r.POST("/restaurant", CreateRestaurantHandler)

	//Meal
	r.POST("/meal", CreateMealHandler)
	r.GET("/meal/:mealId", GetMealHandler)
	r.PATCH("/meal", UpdateMealHandler)

	//Order
	r.POST("/order/:mealId", CreateOrderHandler)
	r.GET("/order/:mealId/:userId", GetOrderHandler)
	r.PATCH("/order/:mealId", UpdateOrderHandler)

	r.Run()
}

func init(){

	//Connection to Firebase
	ctx = context.Background()
	opt := option.WithCredentialsFile("ab-sushi-firebase.json")
	app, firebaseErr = firebase.NewApp(context.Background(), nil, opt)
	if firebaseErr != nil {
		log.Println(firebaseErr)
	}

	//Firestore
	firestoreClient, firebaseErr = app.Firestore(ctx)
	if firebaseErr != nil {
		log.Println(firebaseErr)
	}

}
