package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"log"
	"encoding/json"
	"github.com/satori/go.uuid"
	"google.golang.org/api/iterator"
)

type Restaurant struct {

	Id 		string		`json:"id,omitempty"`
	Name 	string		`json:"name,omitempty"`
	Menu 	[]*Category	`json:"menu,omitempty"`

}

func CreateRestaurantHandler(c *gin.Context){

	var statusCode int = http.StatusOK
	var restaurant Restaurant

	err := c.Bind(&restaurant)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	restaurant.Id = uuid.NewV4().String()
	setIdOnPieces(&restaurant)

	res, err := firestoreClient.Collection(restaurantsCollectionName).Doc(restaurant.Id).Set(ctx, restaurant)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	c.JSON(statusCode, res)
}

func GetRestaurantHandler(c *gin.Context){

	var restaurantId string = c.Param("restaurantId")
	var restaurant Restaurant
	var statusCode int

	restaurantSnap, err := firestoreClient.Collection(restaurantsCollectionName).Doc(restaurantId).Get(ctx)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	restaurantMap := restaurantSnap.Data()
	restaurantJson, err := json.Marshal(restaurantMap)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	err = json.Unmarshal([]byte(restaurantJson), &restaurant)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	c.JSON(statusCode, restaurant)
}

func GetAllRestaurantsHandler(c *gin.Context) {

	var allRestaurants []Restaurant = make([]Restaurant, 0)
	var statusCode int = http.StatusOK

	res := firestoreClient.Collection(restaurantsCollectionName).Documents(ctx)

	defer res.Stop()
	for {

		var restaurant Restaurant
		restaurantSnap, err := res.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Println(err)
			statusCode = http.StatusInternalServerError
		}

		restaurantMap := restaurantSnap.Data()
		restaurantJson, err := json.Marshal(restaurantMap)
		if err != nil {
			log.Println(err)
			statusCode = http.StatusInternalServerError
		}

		err = json.Unmarshal([]byte(restaurantJson), &restaurant)
		if err != nil {
			log.Println(err)
			statusCode = http.StatusInternalServerError
		}

		allRestaurants = append(allRestaurants, restaurant)

	}


	c.JSON(statusCode, allRestaurants)
}

func setIdOnPieces(restaurant *Restaurant){

	for _, category := range restaurant.Menu {

		category.Id = uuid.NewV4().String()

		for _, piece := range category.Pieces {
			piece.Id = uuid.NewV4().String()
		}

	}

}
