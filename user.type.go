package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"log"
	"encoding/json"
	"cloud.google.com/go/firestore"
	"github.com/satori/go.uuid"
)

type User struct {

	Id      	string	`json:"id"`
	Firstname	string	`json:"firstname"`
	Lastname   	string	`json:"lastname"`
	Email   	string	`json:"email"`

}

func CreateUserHandler(c *gin.Context) {

	var user User
	var statusCode int = http.StatusOK

	err := c.Bind(&user)
	if err != nil {
		log.Println(err)
	}

	user.Id = uuid.NewV4().String()

	res, err := firestoreClient.Collection(usersCollectionName).Doc(user.Id).Set(ctx, user)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	c.JSON(statusCode, res)
}

func GetUserHandler(c *gin.Context){

	var statusCode int = http.StatusOK
	var userId string = c.Param("userId")
	var user User

	userSnap, err := firestoreClient.Collection(usersCollectionName).Doc(userId).Get(ctx)
	if err != nil {
		log.Println(err)
	}

	userMap := userSnap.Data()
	userJson, err := json.Marshal(userMap)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	err = json.Unmarshal([]byte(userJson), &user)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	c.JSON(statusCode, user)
}

func UpdateUserHandler(c *gin.Context)  {

	var statusCode int = http.StatusOK
	var user User

	err := c.Bind(&user)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	var allUpdates []firestore.Update = []firestore.Update{
		{Path:"email", Value:user.Email},
		{Path:"firstname", Value:user.Firstname},
		{Path:"lastname", Value:user.Lastname},
	}

	res, err := firestoreClient.Collection(usersCollectionName).Doc(user.Id).Update(ctx, allUpdates)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	c.JSON(statusCode, res)
}
