package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"log"
	"github.com/satori/go.uuid"
	"encoding/json"
	"google.golang.org/api/iterator"
	"cloud.google.com/go/firestore"
)

type Order struct {

	Id 				string `json:"id"`
	UserId 			string `json:"userId"`

	PiecesOrder 	[]struct{
		PieceId			string `json:"pieceId"`
		Quantity 		int `json:"quantity"`
	} `json:"piecesOrder"`

}

func CreateOrderHandler(c *gin.Context){

	var statusCode int = http.StatusOK
	var order Order
	var mealId string = c.Param("mealId")

	err := c.Bind(&order)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	order.Id = uuid.NewV4().String()

	res, err := firestoreClient.Collection(mealsCollectionName).Doc(mealId).Collection(ordersCollectionName).Doc(order.Id).Set(ctx, order)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	c.JSON(statusCode, res)
}

func GetOrderHandler(c *gin.Context){

	var order Order

	var statusCode  int = http.StatusOK
	var mealId    	string = c.Param("mealId")
	var userId   	string = c.Param("userId")

	res := firestoreClient.Collection(mealsCollectionName).Doc(mealId).Collection(ordersCollectionName).
		Where("UserId", "==", userId).Documents(ctx)

	defer res.Stop()
	for {

		orderSnap, err := res.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Println(err)
			statusCode = http.StatusInternalServerError
		}

		orderMap := orderSnap.Data()
		orderJson, err := json.Marshal(orderMap)
		if err != nil {
			log.Println(err)
			statusCode = http.StatusInternalServerError
		}

		err = json.Unmarshal([]byte(orderJson), &order)
		if err != nil {
			log.Println(err)
			statusCode = http.StatusInternalServerError
		}

	}

	c.JSON(statusCode, order)
}

func UpdateOrderHandler(c *gin.Context){

	var statusCode int = http.StatusOK
	var order Order
	var mealId    	string = c.Param("mealId")

	err := c.Bind(&order)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	var allUpdates []firestore.Update = []firestore.Update{
		{Path:"PiecesOrder", Value:order.PiecesOrder},
	}

	res, err := firestoreClient.Collection(mealsCollectionName).Doc(mealId).Collection(ordersCollectionName).Doc(order.Id).Update(ctx, allUpdates)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	c.JSON(statusCode, res)
}
