package main

import (
	"github.com/gin-gonic/gin"
	"database/sql"
	"log"
	"net/http"
)

type Piece struct {
	Id 				string 	`json:"id,omitempty"`
	Name 			string 	`json:"name,omitempty"`
}

func GetAllPiecesHandler(c *gin.Context){

	var allPieces []Piece = make([]Piece, 0)

	var statusCode int

	db, err := sql.Open("mysql", stringConnect)
	if err != nil {
		log.Println("an error occured during the connection")
	}
	defer db.Close()

	var sqlQuery        string = `SELECT * FROM piece`

	rows, errInsert := db.Query(sqlQuery)
	if errInsert != nil {

		statusCode = http.StatusInternalServerError
		log.Println("an error occured during the query")

	} else {

		for rows.Next() {

			var piece Piece

			err = rows.Scan(
				&piece.Id,
				&piece.Name,
			)

			allPieces = append(allPieces, piece)
		}

		statusCode = http.StatusOK
	}

	c.JSON(statusCode, allPieces)
}