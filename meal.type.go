package main

import (
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"net/http"
	"log"
	"github.com/satori/go.uuid"
	"encoding/json"
	"cloud.google.com/go/firestore"
)

type Meal struct {

	Id 				string	`json:"id"`
	UserId 			string	`json:"userId"`
	RestaurantId 	string	`json:"restaurantId"`
	Date 			string	`json:"date"`
	Active 			bool	`json:"active"`
	Number 			int		`json:"number"`
	MailingDate 	string	`json:"mailingDate"`

}

func CreateMealHandler(c *gin.Context){

	var statusCode int = http.StatusOK
	var meal Meal

	err := c.Bind(&meal)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	meal.Id = uuid.NewV4().String()

	_, err = firestoreClient.Collection(mealsCollectionName).Doc(meal.Id).Set(ctx, meal)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	c.JSON(statusCode, meal.Id)
}

func GetMealHandler(c *gin.Context){

	var statusCode int = http.StatusOK
	var meal    Meal
	var mealId  string = c.Param("mealId")

	userSnap, err := firestoreClient.Collection(mealsCollectionName).Doc(mealId).Get(ctx)
	if err != nil {
		log.Println(err)
	}

	userMap := userSnap.Data()
	userJson, err := json.Marshal(userMap)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	err = json.Unmarshal([]byte(userJson), &meal)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	c.JSON(statusCode, meal)
}

func UpdateMealHandler(c *gin.Context)  {

	var statusCode int = http.StatusOK
	var meal Meal

	err := c.Bind(&meal)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	var allUpdates []firestore.Update = []firestore.Update{
		{Path:"Number", Value:meal.Number},
		{Path:"Active", Value:meal.Active},
		{Path:"Date", Value:meal.Date},
		{Path:"MailingDate", Value:meal.MailingDate},
	}

	res, err := firestoreClient.Collection(mealsCollectionName).Doc(meal.Id).Update(ctx, allUpdates)
	if err != nil {
		log.Println(err)
		statusCode = http.StatusInternalServerError
	}

	c.JSON(statusCode, res)
}
